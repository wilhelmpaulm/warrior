
public class MapTile {

    protected String imageName;
    protected int x;
    protected int y;

    final int mapColumns = 10;

    public String getImageName() {
        return this.imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getMapIndex() {
        return (this.y * this.mapColumns) + this.x;
    }
}
