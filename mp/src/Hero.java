
public class Hero extends Character {

    private Weapon weapon;
    private Armor armor;

    public Hero(String name, String description, float health, float attack, float defense, float speed) {
        super(name, description, health, attack, defense, speed);
    }

    public Hero(String name, String description, float health, float attack, float defense, float speed, String imageName) {
        super(name, description, health, attack, defense, speed, imageName);
    }

    public Hero(Weapon weapon, Armor armor, String name, String description, float health, float attack, float defense, float speed) {
        super(name, description, health, attack, defense, speed);
        this.weapon = weapon;
        this.armor = armor;
    }

    public String equipWeapon(Weapon weapon) {
        this.weapon = weapon;
        return this.name + " equiped " + weapon.getName() + "\n";
    }

    public String equipArmor(Armor armor) {
        this.armor = armor;
        return this.name + " equiped " + armor.getName() + "\n";
    }

    @Override
    protected float getAttack() {
        float totalAttack = this.weapon != null ? this.attack + this.weapon.getAttack() : this.attack;
        return this.isCharged ? totalAttack * 3 : totalAttack;
    }

    @Override
    protected float getDefense() {
        return this.armor != null ? this.defense + this.armor.getDefense() : this.defense;
    }

    @Override
    public float getSpeed() {
        float totalSpeed = this.speed;
        totalSpeed -= this.armor != null ? this.armor.getSpeedPenalty() : 0;
        totalSpeed -= this.weapon != null ? this.weapon.getSpeedPenalty() : 0;
        return totalSpeed;
    }

    @Override
    public void printInfo() {
        // this justs prints the info of the hero
        // the diff from character implementation is
        // this has weapon and armor equipped
        String stats = "";
        stats += "|------------------------" + "\n";
        stats += "| name: " + this.name + "\n";
        stats += "|------------------------" + "\n";
        stats += "| wpn: " + this.weapon.getName() + " ";
        stats += "| arm: " + this.armor.getName() + "\n";
        stats += "|------------------------" + "\n";
        stats += "| hp: " + this.getHealth() + " ";
        stats += "| atk: " + this.getAttack() + " ";
        stats += "| def: " + this.getDefense() + " ";
        stats += "| spd: " + this.getSpeed() + "\n";
        System.out.println(stats);
    }

    @Override
    public String toString() {
        String stats = "";
        stats += this.name + "\n";
        stats += this.weapon != null ? "WPN: " + this.weapon.getName() + "\n" : "";
        stats += this.armor != null ? "ARM: " + this.armor.getName() + "\n" : "";
        stats += "HP : " + this.getHealth() + "\n";
        stats += "ATK: " + this.getAttack() + "\n";
        stats += "DEF: " + this.getDefense() + "\n";
        stats += "SPD: " + this.getSpeed() + "\n";
        return stats;
    }
}
