
public class Arena extends Environment {

    public Arena(String name, String description) {
        super(name, description);
    }

    public Arena(String name, String description, String imageName) {
        super(name, description, imageName);
    }

    @Override
    public void applyEffect(Character character) {
        // this environment has no effect
    }
}
