
public class Weapon extends MapTile {

    private String name;
    private String description;

    private float attack;
    private float speedPenalty;

    public Weapon(String name, String description, float attack, float speedPenalty) {
        this.name = name;
        this.description = description;
        this.attack = attack;
        this.speedPenalty = speedPenalty;
    }

    public Weapon(String name, String description, float attack, float speedPenalty, String imageName) {
        this.name = name;
        this.description = description;
        this.attack = attack;
        this.speedPenalty = speedPenalty;
        this.imageName = imageName;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public float getAttack() {
        return attack;
    }

    public float getSpeedPenalty() {
        return speedPenalty;
    }

}
