
public class Armor extends MapTile {

    private String name;
    private String description;

    private float defense;
    private float speedPenalty;

    public Armor(String name, String description, float defense, float speedPenalty) {
        this.name = name;
        this.description = description;
        this.defense = defense;
        this.speedPenalty = speedPenalty;
    }

    public Armor(String name, String description, float defense, float speedPenalty, String imageName) {
        this.name = name;
        this.description = description;
        this.defense = defense;
        this.speedPenalty = speedPenalty;
        this.imageName = imageName;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public float getDefense() {
        return defense;
    }

    public float getSpeedPenalty() {
        return speedPenalty;
    }

}
