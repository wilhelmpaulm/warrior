
public abstract class Environment extends MapTile {

    private String name;
    private String description;

    public Environment(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Environment(String name, String description, String imageName) {
        this.name = name;
        this.description = description;
        this.imageName = imageName;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public abstract void applyEffect(Character character);

    @Override
    public String toString() {
        String stats = "";
        stats += this.getName() + "\n";
        stats += this.getDescription() + "\n";
        return stats;
    }

}
