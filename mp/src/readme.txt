externals:

- Scroll-o-Sprites
- - The Scroll-o-Sprites is a free asset library geared toward Roguelike developers.
- - https://www.reddit.com/r/roguelikedev/comments/1dmbxr/art_scrollosprites/
- - http://imgur.com/a/uHx4k

- Tiled Map Editor
- - Tiled is a free software level editor. It supports editing tile maps in various projections (orthogonal, isometric, hexagonal) and also supports building levels with freely positioned, rotated or scaled images or annotating them with objects of various shapes.
- - http://www.mapeditor.org/
- - https://thorbjorn.itch.io/tiled