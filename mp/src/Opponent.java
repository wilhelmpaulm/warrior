
import java.util.ArrayList;

public class Opponent extends Character {

    private ArrayList<String> thinkList;
    private int level;
    // the thinkList will contain the possible actions
    // and also the order how those actions would be executed
    // 1 = attack
    // 2 = defend
    // 3 = charge

    public Opponent(String name, String description, float health, float attack, float defense, float speed) {
        super(name, description, health, attack, defense, speed);
        this.setThinkPattern();
    }

    public Opponent(String name, String description, float health, float attack, float defense, float speed, int level) {
        super(name, description, health, attack, defense, speed);
        this.level = level;
        this.setThinkPattern();
    }

    public Opponent(String name, String description, float health, float attack, float defense, float speed, int level, String imageName) {
        super(name, description, health, attack, defense, speed, imageName);
        this.level = level;
        this.setThinkPattern();
    }

    private void setThinkPattern() {
        this.thinkList = new ArrayList<>();
        if (this.level == 2) {
            // can attack and defend
            thinkList.add("1");
            thinkList.add("2");
            thinkList.add("1");
        } else if (this.level == 3) {
            // can attack and charge
            thinkList.add("1");
            thinkList.add("2");
            thinkList.add("1");
        } else {
            // can only attack
            thinkList.add("1");
        }
    }

    public String think() {
        String nextAction = this.thinkList.get(0);
        this.thinkList.remove(0);
        this.thinkList.add(nextAction);
        return nextAction;
    }

    @Override
    public void reset() {
        super.reset();
        this.setThinkPattern();
    }
    
    

}
