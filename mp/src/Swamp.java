
public class Swamp extends Environment {

    public Swamp(String name, String description) {
        super(name, description);
    }

    public Swamp(String name, String description, String imageName) {
        super(name, description, imageName);
    }

    @Override
    public void applyEffect(Character character) {
        if (character.getClass().equals(Hero.class)) {
            character.setHealth(character.getHealth() - 1);
        }

        if (character.getClass().equals(Opponent.class)) {
            character.setAttack(character.getAttack() + 1);
        }
    }

}
