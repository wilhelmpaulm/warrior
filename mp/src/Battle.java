
import java.util.ArrayList;
import java.util.Scanner;

public class Battle {

    private ArrayList<Environment> environments;
    private ArrayList<Opponent> opponents;
    private ArrayList<Weapon> weapons;
    private ArrayList<Armor> armors;

    private Opponent opponent;
    private Hero hero;

    private String heroOption;
    private String opponentOption;
    private Environment environment;

    int round = 0;
    Scanner scanner;
    String option = "";
    boolean hasFleedBattle = false;
    boolean isCorrectInput = false;

    public Battle() {
        this.scanner = new Scanner(System.in);

        this.environments = new ArrayList<>();
        this.weapons = new ArrayList<>();
        this.armors = new ArrayList<>();
        this.opponents = new ArrayList<>();
    }

    public Battle(Hero hero) {
        this.scanner = new Scanner(System.in);

        this.hero = hero;

        this.environments = new ArrayList<>();
        this.weapons = new ArrayList<>();
        this.armors = new ArrayList<>();
        this.opponents = new ArrayList<>();
    }

    public void addEnvironment(Environment environment) {
        this.environments.add(environment);
    }

    public void addOpponent(Opponent opponent) {
        this.opponents.add(opponent);
    }

    public void addWeapon(Weapon weapon) {
        this.weapons.add(weapon);
    }

    public void addArmor(Armor armor) {
        this.armors.add(armor);
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public void setOpponent(Opponent opponent) {
        this.opponent = opponent;
    }

    public void printInfo() {
        System.out.println("|");
        System.out.println("|------------------------------------------------");
        System.out.println("| Displaying battle info");
        System.out.println("|------------------------------------------------");
        System.out.println("");
        this.hero.printInfo();
        this.opponent.printInfo();
        System.out.println("|------------------------------------------------");
    }

    public void chooseArmor() {
        // print the armor menu
        System.out.println("|");
        System.out.println("|------------------------------------------------");
        System.out.println("| Select your armor!");
        System.out.println("|------------------------------------------------");
        for (Armor armor : this.armors) {
            System.out.println("| [" + this.armors.indexOf(armor) + "] - " + armor.getName() + " | " + armor.getDescription());
        }
        System.out.println("|");

        isCorrectInput = false;

        // while the input is invalid
        // loop through the options
        while (!isCorrectInput) {
            isCorrectInput = false;
            option = "";

            System.out.print("| Your chosen armor? (input option then press enter): ");
            if (scanner.hasNext()) {
                option = scanner.next();
            } else {
                System.out.println("|");
                System.out.println("| Please input an int");
                System.out.println("|");
            }

            for (Armor armor : this.armors) {
                int index = this.armors.indexOf(armor);
                if (option.equalsIgnoreCase(index + "")) {
                    this.hero.equipArmor(armor);
                    isCorrectInput = true;
                    break;
                }
            }
            if (!isCorrectInput) {
                System.out.println("|");
                System.out.println("| Please select a valid armor ");
                System.out.println("|");
            }
        }
    }

    public void chooseWeapon() {
        // print the armor menu
        System.out.println("|");
        System.out.println("|------------------------------------------------");
        System.out.println("| Select your weapon!");
        System.out.println("|------------------------------------------------");
        for (Weapon weapon : this.weapons) {
            System.out.println("| [" + this.weapons.indexOf(weapon) + "] - " + weapon.getName() + " | " + weapon.getDescription());
        }
        System.out.println("|");

        isCorrectInput = false;

        // while the input is invalid
        // loop through the options
        while (!isCorrectInput) {
            isCorrectInput = false;
            option = "";

            System.out.print("| Your chosen weapon? (input option then press enter): ");
            if (scanner.hasNext()) {
                option = scanner.next();
            } else {
                System.out.println("|");
                System.out.println("| Please input an int");
                System.out.println("|");
            }

            for (Weapon weapon : this.weapons) {
                int index = this.weapons.indexOf(weapon);
                if (option.equalsIgnoreCase(index + "")) {
                    this.hero.equipWeapon(weapon);
                    isCorrectInput = true;
                    break;
                }
            }
            if (!isCorrectInput) {
                System.out.println("|");
                System.out.println("| Please select a valid weapon ");
                System.out.println("|");
            }
        }
    }

    public void chooseEnvironment() {
        // print the armor menu
        System.out.println("|");
        System.out.println("|------------------------------------------------");
        System.out.println("| Select the environment");
        System.out.println("|------------------------------------------------");
        for (Environment environment : this.environments) {
            System.out.println("| [" + this.environments.indexOf(environment) + "] - " + environment.getName() + " | " + environment.getDescription());
        }
        System.out.println("|");

        isCorrectInput = false;

        // while the input is invalid
        // loop through the options
        while (!isCorrectInput) {
            isCorrectInput = false;
            option = "";

            System.out.print("| Your chosen environment! (input option then press enter): ");
            if (scanner.hasNext()) {
                option = scanner.next();
            } else {
                System.out.println("|");
                System.out.println("| Please input an int");
                System.out.println("|");
            }

            for (Environment environment : this.environments) {
                int index = this.environments.indexOf(environment);
                if (option.equalsIgnoreCase(index + "")) {
                    this.setEnvironment(environment);
                    isCorrectInput = true;
                    break;
                }
            }
            if (!isCorrectInput) {
                System.out.println("|");
                System.out.println("| Please select a valid environment ");
                System.out.println("|");
            }
        }
        System.out.println("|------------------------------------------------");
        System.out.println("| " + this.hero.getName() + " will be fighting in " + this.environment.getName());
    }

    public void chooseOpponent() {
        // print the armor menu
        System.out.println("|");
        System.out.println("|------------------------------------------------");
        System.out.println("| Select the opponent");
        System.out.println("|------------------------------------------------");
        for (Character character : this.opponents) {
            System.out.println("| [" + this.opponents.indexOf(character) + "] - " + character.getName() + " | " + character.getDescription());
        }
        System.out.println("|");

        isCorrectInput = false;

        // while the input is invalid
        // loop through the options
        while (!isCorrectInput) {
            isCorrectInput = false;
            option = "";

            System.out.print("| Your chosen opponent! (input option then press enter): ");
            if (scanner.hasNext()) {
                option = scanner.next();
            } else {
                System.out.println("|");
                System.out.println("| Please input an int");
                System.out.println("|");
            }

            for (Opponent opponent : this.opponents) {
                int index = this.opponents.indexOf(opponent);
                if (option.equalsIgnoreCase(index + "")) {
                    this.setOpponent(opponent);
                    isCorrectInput = true;
                    break;
                }
            }
            if (!isCorrectInput) {
                System.out.println("|");
                System.out.println("| Please select a valid opponent ");
                System.out.println("|");
            }
        }
        System.out.println("|------------------------------------------------");
        System.out.println("| " + this.hero.getName() + " wants to fight " + this.opponent.getName());
    }

    public String getHeroTurnAction() {
        // prepare for input
        isCorrectInput = false;
        String option = "";

        // print info
        System.out.println("|------------------------------------------------");
        System.out.println("| Your turn hero:");

        // print hero actions
        if (!this.hero.isCharged) {
            System.out.println("| [1] - Attack " + this.opponent.getName());
        } else {
            System.out.println("| [1] - Charged attack " + this.opponent.getName());
        }

        if (!this.hero.isDefending) {
            System.out.println("| [2] - Defend");
        }

        if (!this.hero.isCharged) {
            System.out.println("| [3] - Charge");
        }
        System.out.println("| [4] - Show battle info");
        System.out.println("| [5] - Flee");
        System.out.println("|------------------------------------------------");

        while (!isCorrectInput) {
            isCorrectInput = true;
            option = "";

            System.out.print("| Your action? (input int then enter): ");
            if (scanner.hasNext()) {
                option = scanner.next();
            } else {
                System.out.println("|");
                System.out.println("| Please input a int");
                System.out.println("|");
            }

            if ("4".equalsIgnoreCase(option)) {
                this.printInfo();
            }

            if ("1 2 3 5".indexOf(option) > -1) {
                isCorrectInput = true;
                break;
            } else {
                isCorrectInput = false;
                System.out.println("|");
                System.out.println("| Please select a valid action ");
                System.out.println("|");
            }
        }
        return option.trim();
    }

    public String getOpponentAction() {
        // 1 is for attack
        // 2 is for defend
        // 3 is for charge
        return this.opponent.think();
    }

    public void applyAction(Character actor, Character receiver, String option) {
        if (actor.isDead() || receiver.isDead()) {
            this.end();
        }

        if (option.equalsIgnoreCase("1")) {
            actor.attack(receiver);
        } else if (option.equalsIgnoreCase("2")) {
            actor.defend();
        } else if (option.equalsIgnoreCase("3") && !actor.isCharged) {
            actor.charge();
        } else if (option.equalsIgnoreCase("5")) {
            this.flee();
        }
    }

    public void prepare() {
        this.chooseArmor();
        this.chooseWeapon();
        this.chooseEnvironment();
        this.chooseOpponent();
    }

    public void start() {
        this.prepare();
        // this is a simple one on one battle
        // while no one is dead
        // each loop is one round
        System.out.println("|------------------------------------------------");
        System.out.println("| " + this.hero.getName() + " vs " + this.opponent.getName());
        while (!this.hero.isDead() && !this.opponent.isDead() && !this.hasFleedBattle) {
            // add 1 to round
            this.round += 1;

            // print info
            System.out.println("|");
            System.out.println("|------------------------------------------------");
            System.out.println("| round " + this.round);

            // apply the environment effects
            this.environment.applyEffect(this.hero);
            this.environment.applyEffect(this.opponent);

            this.heroOption = this.getHeroTurnAction();
            this.opponentOption = this.getOpponentAction();

            if (this.heroOption.equalsIgnoreCase("2") || this.hero.getSpeed() >= this.opponent.getSpeed()) {
                this.applyAction(hero, opponent, heroOption);
                this.applyAction(opponent, hero, opponentOption);
            } else {
                this.applyAction(opponent, hero, opponentOption);
                this.applyAction(hero, opponent, heroOption);
            }
        }
        this.end();
    }

    public void end() {
        // announce the winner or the loser
        System.out.println("|------------------------------------------------");
        if (this.hero.isDead()) {
            System.out.println("| " + this.opponent.getName() + " wins!");
        } else if (this.opponent.isDead()) {
            System.out.println("| " + this.hero.getName() + " wins!");
        }

        // remove buffs and debuffs
        // heal characters for the next battle
        this.hero.reset();
        this.opponent.reset();
    }

    public void flee() {
        this.hasFleedBattle = true;
        this.hero.reset();
        this.opponent.reset();

        System.out.println("|------------------------------------------------");
        System.out.println("| " + this.hero.getName() + " has declared cowardice!");
        System.out.println("| " + this.opponent.getName() + " wins!");

    }

}
