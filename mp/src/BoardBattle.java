
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class BoardBattle {

    private BoardManager boardManager;
    private ArrayList<Object> mapTileObjects;
    int round = 0;

    public BoardBattle(BoardManager boardManager) {
        this.boardManager = boardManager;
        this.mapTileObjects = new ArrayList<>();
    }

    public boolean start() {
        // this is a simple one on one battle
        // while no one is dead
        // each loop is one round
        this.boardManager.hasFleedBattle = false;
        this.boardManager.appendToLog(this.boardManager.hero.getName() + " vs " + this.boardManager.opponent.getName());
        this.boardManager.showMoveButtons(false);

        while (!this.boardManager.hero.isDead() && !this.boardManager.opponent.isDead() && !this.boardManager.hasFleedBattle) {
            // add 1 to round
            this.round += 1;
            this.boardManager.appendToLog("battle round: " + this.round);

            // apply the environment effects
            this.boardManager.environment.applyEffect(this.boardManager.hero);
            this.boardManager.environment.applyEffect(this.boardManager.opponent);

            this.setHeroTurnAction();
            this.setOpponentTurnAction();

            if (this.boardManager.heroOption.equalsIgnoreCase("2") || this.boardManager.hero.getSpeed() >= this.boardManager.opponent.getSpeed()) {
                this.applyAction(this.boardManager.hero, this.boardManager.opponent, this.boardManager.heroOption);
                this.applyAction(this.boardManager.opponent, this.boardManager.hero, this.boardManager.opponentOption);
            } else {
                this.applyAction(this.boardManager.opponent, this.boardManager.hero, this.boardManager.opponentOption);
                this.applyAction(this.boardManager.hero, this.boardManager.opponent, this.boardManager.heroOption);
            }
        }
        return this.end();
    }

    public boolean end() {
        // announce the winner or the loser
        boolean heroWon = false;
        if (this.boardManager.hero.isDead()) {
            this.boardManager.appendToLog(this.boardManager.opponent.getName() + " wins!");
            JOptionPane.showMessageDialog(null, "Hero lost . . .");
            heroWon = false;
        } else if (this.boardManager.opponent.isDead()) {
            this.boardManager.appendToLog(this.boardManager.hero.getName() + " wins!");
            JOptionPane.showMessageDialog(null, "Hero won!");
            heroWon = true;
        }
        this.boardManager.setMapTileIcon(this.boardManager.tileOpponent, "0");
        this.boardManager.textAreaOpponent.setText("");

        // remove buffs and debuffs
        // heal characters for the next battle
        this.boardManager.hero.reset();
        this.boardManager.textAreaHero.setText(this.boardManager.hero.toString());
        this.boardManager.showMoveButtons(true);
        return heroWon;
    }

    public void flee() {
        this.boardManager.hasFleedBattle = true;
        this.boardManager.hero.reset();
        this.boardManager.opponent.reset();

        this.boardManager.appendToLog(this.boardManager.hero.getName() + " has declared cowardice!");
        this.boardManager.appendToLog(this.boardManager.opponent.getName() + " wins!");

        this.boardManager.showMoveButtons(true);
    }

    public void setHeroTurnAction() {
        String[] options = new String[] {"Attack", "Defend", "Charge", "Flee"};
        int response = JOptionPane.showOptionDialog(null, "What's your next move?", "Hero action",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
        this.boardManager.heroOption = (response + 1) + "";
    }

    public void setOpponentTurnAction() {
        this.boardManager.opponentOption = this.boardManager.opponent.think();
    }

    public void applyAction(Character actor, Character receiver, String option) {
        if (actor.isDead() || receiver.isDead()) {
            this.end();
        }

        if (option.equalsIgnoreCase("1")) {
            this.boardManager.appendToLog(actor.attack(receiver));
        } else if (option.equalsIgnoreCase("2")) {
            this.boardManager.appendToLog(actor.defend());
        } else if (option.equalsIgnoreCase("3") && !actor.isCharged) {
            this.boardManager.appendToLog(actor.charge());
        } else if (option.equalsIgnoreCase("4")) {
            this.flee();
        }
        this.boardManager.textAreaHero.setText(this.boardManager.hero.toString());
        this.boardManager.textAreaOpponent.setText(this.boardManager.opponent.toString());
    }

}
