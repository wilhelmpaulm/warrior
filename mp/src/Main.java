
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        // the big bang creates random places
        Arena arena = new Arena("arena arena", "does nothing to characters", "environment_arena");
        Arena arena2 = new Arena("arena arena", "does nothing to characters", "environment_arena");
        Swamp swamp = new Swamp("swamp", "penalizes hero ", "environment_swamp");
        Swamp swamp2 = new Swamp("swamp", "oh this isn't a swamp!?", "environment_swamp");
        Colosseum colosseum = new Colosseum("colosseum", "close enought . . .", "environment_colosseum");
        Colosseum colosseum2 = new Colosseum("colosseum", "close enought . . .", "environment_colosseum");

        // craft weapons
        Weapon dagger = new Weapon("dagger", "fast small slashy thing", 20, 0, "weapon_dagger");
        Weapon sword = new Weapon("sword", "long pointy slashy thing", 30, 10, "weapon_sword");
        Weapon batteAxe = new Weapon("Battle Axe", "big slashy show thing", 40, 20, "weapon_battleaxe");

        // polish the armors
        Armor lightArmor = new Armor("light armor", "this little piggy wore cotton", 20, 5, "armor_light");
        Armor mediumArmor = new Armor("medium armor", "this little piggy wore wood", 30, 15, "armor_medium");
        Armor heavyrmor = new Armor("heavy armor", "this little piggy wore metal", 40, 25, "armor_heavy");

        // post the wanted posters
        // opponent level is set at the end of the constructor
        // 1 = attack continously
        // 2 = attack, defend, attack
        // 3 = attack, charge, attack0
        Opponent thief = new Opponent("thief", "pork barrel canon", 150, 20, 20, 40, 1, "opponent_thief");
        Opponent thief2 = new Opponent("thief2", "pork barrel canon", 150, 20, 20, 40, 1, "opponent_thief");
        Opponent viking = new Opponent("viking", "food poisoning on the seven seas", 250, 30, 30, 30, 2, "opponent_viking");
        Opponent viking2 = new Opponent("viking2", "food poisoning on the seven seas", 250, 30, 30, 30, 2, "opponent_viking");
        Opponent minotaur = new Opponent("minotaur", "24/7 energy, then you die", 350, 40, 40, 20, 3, "opponent_minotaur");

        // comb that hero hair
        Hero hero = new Hero("Victor", "becasue ice ice baby", 100, 1, 1, 50, "hero");
        // weild your cheap weapon
//        hero.equipWeapon(dagger); // equip weapon
        // wear your tattered armor
//        hero.equipArmor(lightArmor); // equip armor

        // start an encounter
//        Battle battle = new Battle(arena, hero, thief);
//        battle.start();
//        Battle battle = new Battle(hero);
//        battle.addWeapon(sword);
//        battle.addWeapon(dagger);
//        battle.addWeapon(batteAxe);
//
//        battle.addArmor(heavyrmor);
//        battle.addArmor(lightArmor);
//        battle.addArmor(mediumArmor);
//
//        battle.addEnvironment(arena);
//        battle.addEnvironment(swamp);
//        battle.addEnvironment(colosseum);
//
//        battle.addOpponent(thief);
//        battle.addOpponent(viking);
//        battle.addOpponent(minotaur);
//
//        battle.start();
        // create a board and run the GUI
//        Board board = new Board();
//        ArrayList<Armor> armors = new ArrayList<>();
//        lightArmor.setXY(0, 9); //0 (10) + 9 =9
//        armors.add(lightArmor);
//        mediumArmor.setXY(5, 4);
//        armors.add(mediumArmor);
//        heavyrmor.setXY(2, 8);
//        armors.add(heavyrmor);
//
//        ArrayList<Weapon> weapons = new ArrayList<>();
//        sword.setXY(3, 0);
//        weapons.add(sword);
//        dagger.setXY(2, 2);
//        weapons.add(dagger);
//        batteAxe.setXY(0, 4);
//        weapons.add(batteAxe);
//
//        ArrayList<Opponent> opponents = new ArrayList<>();
//        thief.setXY(7, 4);
//        opponents.add(thief);
//        thief2.setXY(3, 7);
//        opponents.add(thief2);
//        minotaur.setXY(7, 7);
//        opponents.add(minotaur);
//        viking.setXY(7, 9);
//        opponents.add(viking);
//        viking2.setXY(4, 1);
//        opponents.add(viking2);
        BoardManager boardManager = new BoardManager(hero, 0, 0);
        boardManager.setEnvironment(arena);
        boardManager.addMapTile(arena2, 8, 9);
        boardManager.addMapTile(swamp, 0, 7);
        boardManager.addMapTile(swamp2, 7, 5);
        boardManager.addMapTile(colosseum, 5, 7);
        boardManager.addMapTile(colosseum2, 8, 7);
        boardManager.addMapTile(lightArmor, 0, 9);
        boardManager.addMapTile(mediumArmor, 5, 4);
        boardManager.addMapTile(heavyrmor, 2, 8);
        boardManager.addMapTile(sword, 3, 0);
        boardManager.addMapTile(dagger, 2, 2);
        boardManager.addMapTile(batteAxe, 0, 4);
        boardManager.addMapTile(thief, 7, 4);
        boardManager.addMapTile(thief2, 3, 7);
        boardManager.addMapTile(minotaur, 7, 7);
        boardManager.addMapTile(viking, 7, 9);
        boardManager.addMapTile(viking2, 4, 1);
        boardManager.displayBoard();
    }
}
