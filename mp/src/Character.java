
public abstract class Character extends MapTile {

    protected String name;
    protected String description;

    protected float health;
    protected float attack;
    protected float defense;
    protected float speed;

    protected float baseHealth;
    protected float baseAttack;
    protected float baseDefense;
    protected float baseSpeed;

    protected boolean isCharged;
    protected boolean isDefending;

    public Character(String name, String description, float health, float attack, float defense, float speed) {
        this.name = name;
        this.description = description;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.speed = speed;

        // these are set so we know the real base stats
        // of the character when they were created
        // this gives us a point to return to
        // when we want to remove all the buffs, debuffs
        // from a battle
        this.baseHealth = health;
        this.baseAttack = attack;
        this.baseDefense = defense;
        this.baseSpeed = speed;
    }

    public Character(String name, String description, float health, float attack, float defense, float speed, String imageName) {
        this.name = name;
        this.description = description;
        this.imageName = imageName;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.speed = speed;

        // these are set so we know the real base stats
        // of the character when they were created
        // this gives us a point to return to
        // when we want to remove all the buffs, debuffs
        // from a battle
        this.baseHealth = health;
        this.baseAttack = attack;
        this.baseDefense = defense;
        this.baseSpeed = speed;
    }

    public String attack(Character opponent) {
        // print info
        String returnMessage = this.name + " attacks " + opponent.getName() + "\n";
        // this accepts a character or
        // any children of the Characeter class
        returnMessage += opponent.takeDamage(this.getAttack());
        this.isCharged = false;
        return returnMessage;
    }

    public String charge() {
        // on charge disable defense
        this.isCharged = true;
        this.isDefending = false;

        // print info
        return this.name + " charges ";
    }

    public String defend() {
        // on defend still maintain the state of charge
        this.isDefending = true;
        return this.name + " goes on the defensive" + "\n";
    }

    public String heal(float damage) {
        // just the basic heal
        this.health += damage;

        // print info
        return this.name + " heals for " + damage + " damage " + "\n";
    }

    public String takeDamage(float damage) {
        // damage is weakened by the defense
        // if defending, the damage gets halved
        // else maintain the damage
        damage -= this.getDefense();
        damage = this.isDefending ? damage / 2 : damage;
        damage = (damage > 0) ? damage : 0;
        // if the damage is above 0 then apply damage
        this.health -= damage;
        // if the character's health is negative just return to zero
        if (this.health < 0) {
            this.health = 0;
        }
        // the character that was defending during the attack
        // would get returned to the normal stance
        this.isDefending = false;

        // print info
        String returnMessage = "";
        returnMessage += this.name + " takes " + damage + " damage (HP: " + this.health + ")" + "\n";
        if (this.isDead()) {
            returnMessage += this.name + " dies" + "\n";
        }
        return returnMessage;
    }

    public void reset() {
        this.health = this.baseHealth;
        this.attack = this.baseAttack;
        this.defense = this.baseDefense;
        this.speed = this.baseSpeed;
        this.isCharged = false;
        this.isDefending = false;
    }

    protected boolean isDead() {
        // this should help decide if the character
        // would still be able to do an action
        return this.health <= 0;
    }

    protected float getAttack() {
        // times 3 if the character is charged
        return this.isCharged ? this.attack * 3 : this.attack;
    }

    protected float getDefense() {
        return this.defense;
    }

    public float getHealth() {
        return this.health;
    }

    public float getSpeed() {
        return this.speed;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public void setAttack(float attack) {
        this.attack = attack;
    }

    public void setDefense(float defense) {
        this.defense = defense;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void printInfo() {
        // this justs prints the info of the character
        String stats = "";
        stats += "|------------------------" + "\n";
        stats += "| name: " + this.name + "\n";
        stats += "|------------------------" + "\n";
        stats += "| hp: " + this.getHealth() + " ";
        stats += "| atk: " + this.getAttack() + " ";
        stats += "| def: " + this.getDefense() + " ";
        stats += "| spd: " + this.getSpeed() + "\n";
        System.out.println(stats);
    }

    @Override
    public String toString() {
        String stats = "";
        stats += this.name + "\n";
        stats += "HP : " + this.getHealth() + "\n";
        stats += "ATK: " + this.getAttack() + "\n";
        stats += "DEF: " + this.getDefense() + "\n";
        stats += "SPD: " + this.getSpeed() + "\n";
        return stats;
    }

}
