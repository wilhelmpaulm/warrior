
public class Colosseum extends Environment {

    public Colosseum(String name, String description) {
        super(name, description);
    }

    public Colosseum(String name, String description, String imageName) {
        super(name, description, imageName);
    }

    @Override
    public void applyEffect(Character character) {
        if (character.getClass().equals(Hero.class)) {
            character.setAttack(character.getAttack() + 1);
        }

        if (character.getClass().equals(Opponent.class)) {
            character.setDefense(character.getDefense() - 1);
        }
    }

}
