
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class BoardManager extends Board {

    private ArrayList<MapTile> mapTiles = new ArrayList<>();

    protected Hero hero;
    protected Opponent opponent;
    protected Environment environment;

    protected String heroOption;
    protected String opponentOption;
    protected boolean hasFleedBattle;

    final int mapColumns = 10;

    String[] mapDesign;

    public BoardManager(Hero hero, int x, int y) {
        this.hero = hero;
        this.hero.setXY(x, y);
        this.setMapDesign();
        this.addMouseListeners();
    }

    public void addMapTile(MapTile mapTile, int x, int y) {
        mapTile.setXY(x, y);
        this.mapTiles.add(mapTile);
    }

    public void displayBoard() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            System.out.println("look and feel not supported");
        }
        this.setVisible(true);
        this.addTilesToMap();
        this.drawMap();
    }

    private void setMapDesign() {
        String[] map = {
            "0", "0", "0", "0", "0", "4", "1", "1", "1", "5",
            "0", "1", "1", "5", "0", "3", "0", "0", "0", "3",
            "0", "0", "0", "2", "0", "0", "0", "2", "0", "0",
            "1", "0", "1", "11", "1", "1", "10", "11", "5", "0",
            "0", "0", "0", "0", "0", "0", "2", "0", "3", "0",
            "1", "1", "1", "0", "0", "0", "2", "0", "0", "0",
            "0", "0", "0", "0", "4", "1", "12", "1", "1", "0",
            "0", "4", "1", "0", "3", "0", "2", "0", "0", "0",
            "0", "2", "0", "0", "0", "0", "8", "1", "1", "0",
            "0", "6", "1", "1", "1", "1", "7", "0", "0", "0",};
        this.mapDesign = map;
    }

    public void drawMap() {
        int index = 0;
        for (String string : mapDesign) {
            this.setMapTileIcon(index, string);
            index++;
        }
        this.mapPanel.repaint();
    }

    public int convertLocationToIndex(int x, int y) {
        return y * this.mapColumns + x;
    }

    private void setHeroLocation(int x, int y) {
        this.hero.setXY(x, y);
        this.mapDesign[this.hero.getMapIndex()] = this.hero.getImageName();
    }

    public void moveHero(String direction) {
        boolean hasMoved = false;
        int nextX = this.hero.getX();
        int nextY = this.hero.getY();

        if (direction.equalsIgnoreCase("up")) {
            if (nextY > 0) {
                nextY -= 1;
            }
        } else if (direction.equalsIgnoreCase("down")) {
            if (nextY < 9) {
                nextY += 1;
            }
        } else if (direction.equalsIgnoreCase("left")) {
            if (nextX > 0) {
                nextX -= 1;
            }
        } else if (direction.equalsIgnoreCase("right")) {
            if (nextX < 9) {
                nextX += 1;
            }
        } else {
            return;
        }

        int currentIndex = this.convertLocationToIndex(this.hero.getX(), this.hero.getY());
        int nextIndex = this.convertLocationToIndex(nextX, nextY);

        if (!this.isIndexOnMap(nextIndex)) {
            return;
        }
        if (this.mapDesign[nextIndex].equalsIgnoreCase("0") || this.stepOn(nextIndex)) {
            this.mapDesign[currentIndex] = "0";
            this.setHeroLocation(nextX, nextY);
            this.appendToLog("hero moved to the " + direction);
            this.drawMap();
        }
    }

    private boolean isIndexOnMap(int index) {
        return index >= 0 && index < this.mapDesign.length;
    }

    private boolean stepOn(int nextIndex) {
        for (MapTile mapTile : mapTiles) {
            if (mapTile.getMapIndex() != nextIndex) {
                continue;
            }

            System.out.println(mapTile.getClass());

            if (mapTile.getClass().equals(Arena.class)
                    || mapTile.getClass().equals(Swamp.class)
                    || mapTile.getClass().equals(Colosseum.class)) {
                this.environment = (Environment) mapTile;
                this.textAreaEnvironment.setText(this.environment.toString());
                this.setMapTileIcon(this.tileEnvrionment, environment.getImageName());
                this.appendToLog(this.hero.getName() + " has changed the environment to: " + environment.getName());
                this.mapTiles.remove(mapTile);
                return true;
            }

            if (mapTile.getClass().equals(Armor.class)) {
                Armor armor = (Armor) mapTile;
                this.hero.equipArmor(armor);
                this.textAreaHero.setText(this.hero.toString());
                this.setMapTileIcon(this.tileHeroArmor, armor.getImageName());
                this.appendToLog(this.hero.getName() + " equiped: " + armor.getName());
                this.mapTiles.remove(mapTile);
                return true;
            }

            if (mapTile.getClass().equals(Weapon.class)) {
                Weapon weapon = (Weapon) mapTile;
                this.hero.equipWeapon(weapon);
                this.textAreaHero.setText(this.hero.toString());
                this.setMapTileIcon(this.tileHeroWeapon, weapon.getImageName());
                this.appendToLog(this.hero.getName() + " equiped: " + weapon.getName());
                this.mapTiles.remove(mapTile);
                return true;
            }

            if (mapTile.getClass().equals(Opponent.class)) {
                Opponent currentOpponent = (Opponent) mapTile;
                this.textAreaOpponent.setText(currentOpponent.toString());
                this.setMapTileIcon(this.tileOpponent, currentOpponent.getImageName());
                this.appendToLog(this.hero.getName() + " started to battle with " + currentOpponent.getName());
                boolean heroWon = false;
                heroWon = this.startBattle(currentOpponent);
                if (heroWon) {
                    this.mapTiles.remove(mapTile);
                }
                return heroWon;
            }
        }
        return false;
    }

    private void addToMap(MapTile mapTile) {
        this.mapDesign[mapTile.getMapIndex()] = mapTile.getImageName();
    }

    private void addTilesToMap() {

        this.addToMap(this.hero);
        this.textAreaHero.setText(this.hero.toString());

        for (MapTile mapTile : this.mapTiles) {
            this.addToMap(mapTile);
        }

    }

    public void showMoveButtons(boolean show) {
        this.buttonUp.setVisible(show);
        this.buttonDown.setVisible(show);
        this.buttonLeft.setVisible(show);
        this.buttonRight.setVisible(show);
    }

    private void addMouseListeners() {
        buttonUp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                moveHero("up");
            }
        });

        buttonDown.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                moveHero("down");
            }
        });

        buttonLeft.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                moveHero("left");
            }
        });

        buttonRight.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                moveHero("right");
            }
        });

    }

    public boolean startBattle(Opponent opponent) {
        this.opponent = opponent;
        BoardBattle boardBattle = new BoardBattle(this);
        return boardBattle.start();
    }

    public void appendToLog(String log) {
        this.textAreaLog.append(this.getTimeStamp() + " | " + log + "\n");
    }

    public String getTimeStamp() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public JLabel getMapTile(int index) {
        return (JLabel) this.mapPanel.getComponent(index);
    }

    public void setMapTileIcon(int index, String icon) {
        this.getMapTile(index).setIcon(new ImageIcon(getClass().getResource("/assets/sprites/" + icon + ".png")));
    }

    public void setMapTileIcon(JLabel mapTile, String icon) {
        mapTile.setIcon(new ImageIcon(getClass().getResource("/assets/sprites/" + icon + ".png")));
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
        this.setMapTileIcon(this.tileEnvrionment, this.environment.getImageName());
        this.textAreaEnvironment.setText(this.environment.toString());
    }
}
